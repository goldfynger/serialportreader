﻿using System.IO;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;

namespace SerialPortReader
{
    internal class Program
    {
        private const string __usageStr =
            "Usage:\r\n" +
            "  dotnet spreader.dll [-u|--usage] [-v|--version]\r\n" +
            "  dotnet spreader.dll list\r\n" +
            "  dotnet spreader.dll read [-n|--name] [-b|--baudrate] [-d|--databits] [-s|--stopbits] [-p|--parity]\r\n" +
            "  -u --usage:    Shows usage info.\r\n" +
            "  -v --version:  Shows version.\r\n" +
            "  list:          Shows a list of serial ports available on the system.\r\n" +
            "  read:          Reads data from the specified serial port.\r\n" +
            "    -n|--name:      Serial port name.\r\n" +
            "                      Default: first serial port in list.\r\n" +
            "    -b|--baudrate:  Serial port baud rate value.\r\n" +
            "                      Default: 9600.\r\n" +
            "    -d|--databits:  Serial port data bits value.\r\n" +
            "                      Default: 8.\r\n" +
            "                      Allowed: 5 to 8.\r\n" +
            "    -s|--stopbits:  Serial port stop bits value.\r\n" +
            "                      Default: 'One'.\r\n" +
            "                      Allowed: 'One', 'OnePointFive', 'Two'.\r\n" +
            "    -p|--parity:    Serial port parity value.\r\n" +
            "                      Default: 'None'.\r\n" +
            "                      Allowed: 'None', 'Odd', 'Even', 'Mark', 'Space'.";
            
        //DIR: Directory for saving read data. Default: 'C:\\Users\\<username>\\Documents\\spreader' or '/home/<username>/spreader'";

        private const string __badArgsStr =
            "Unable to parse arguments. Run 'dotnet spreader.dll --usage' for usage info.";

        private const string __dateTimeFormat = "HH:mm:ss.ffffff";

        private const string __byteFormat = "X2";

        private const string __decimalFormat = "D8";

        private const int __bytesInLine = 32;


        private enum ExitCode : int
        {
            Success = 0,
            ArgsErr = 1,
            ReadErr = 2
        }


        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                ExitWithCode(ExitCode.ArgsErr, "Run 'dotnet spreader.dll --usage' for usage info.");
            }
            
            var firstArg = args[0];
            
            if (string.IsNullOrWhiteSpace(firstArg))
            {
                ExitWithCode(ExitCode.ArgsErr, __badArgsStr);
            }
            
            switch (firstArg)
            {
                case "-u":
                case "--usage":
                {
                    ExitWithCode(ExitCode.Success, __usageStr);
                }
                break;

                case "-v":
                case "--version":
                {
                    var assembly = AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location);
                    ExitWithCode(ExitCode.Success, $"{assembly.Name} version {assembly.Version}");
                }
                break;

                case "list":
                {
                    ListSerialPorts();
                    ExitWithCode(ExitCode.Success);
                }
                break;
                        
                case "read":
                {
                    ReadSerialPort(args.Skip(1).ToArray());
                    ExitWithCode(ExitCode.Success);
                }
                break;
                        
                default:
                {
                    ExitWithCode(ExitCode.ArgsErr, __badArgsStr);
                }
                break;
            }
        }

        private static void ListSerialPorts()
        {
            string[] portNames = SerialPort.GetPortNames();

            Console.WriteLine("Available serial ports:");

            foreach(string port in portNames)
            {
                Console.WriteLine(port);
            }
        }

        private static void ReadSerialPort(string[] readArgs)
        {
            var portNames = SerialPort.GetPortNames();

            var portName = portNames.FirstOrDefault();
            var baudRate = 9600;
            var dataBits = 8;
            var stopBits = StopBits.One;
            var parity = Parity.None;

            for (var i = 0; i < readArgs.Length; i++)
            {
                switch (readArgs[i])
                {
                    case "-n":
                    case "--name":
                    {
                        var nameArg = readArgs[i + 1];

                        if (portNames.Contains(nameArg))
                        {
                            portName = nameArg;
                        }
                        else
                        {
                            ExitWithCode(ExitCode.ArgsErr, $"Error: invalid port name '{nameArg}'.");
                        }

                        i += 1;
                    }
                    break;

                    case "-b":
                    case "--baudrate":
                    {
                        var baudRateArg = readArgs[i + 1];

                        if (!(int.TryParse(baudRateArg, out baudRate) && baudRate > 0))
                        {
                            ExitWithCode(ExitCode.ArgsErr, $"Error: invalid baud rate '{baudRateArg}'.");
                        }

                        i += 1;
                    }
                    break;

                    case "-d":
                    case "--databits":
                    {
                        var dataBitsArg = readArgs[i + 1];

                        if (!(int.TryParse(dataBitsArg, out dataBits) && baudRate >= 5 && baudRate <= 8))
                        {
                            ExitWithCode(ExitCode.ArgsErr, $"Error: invalid data bits '{dataBitsArg}'.");
                        }

                        i += 1;
                    }
                    break;

                    case "-s":
                    case "--stopbits":
                    {
                        var stopBitsArg = readArgs[i + 1];

                        if (!(StopBits.TryParse(stopBitsArg, out stopBits) && stopBits != StopBits.None))
                        {
                            ExitWithCode(ExitCode.ArgsErr, $"Error: invalid stop bits '{stopBitsArg}'.");
                        }

                        i += 1;
                    }
                    break;

                    case "-p":
                    case "--parity":
                    {
                        var parityArg = readArgs[i + 1];

                        if (!Parity.TryParse(parityArg, out parity))
                        {
                            ExitWithCode(ExitCode.ArgsErr, $"Error: invalid parity '{parityArg}'.");
                        }

                        i += 1;
                    }
                    break;

                    default:
                    {
                        ExitWithCode(ExitCode.ArgsErr, __badArgsStr);
                    }
                    break;
                }
            }

            ReadSerialPort(portName, baudRate, parity, dataBits, stopBits);
        }

        private static void ReadSerialPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
        {
            try
            {
                using (var serialPort = new SerialPort(portName, baudRate, parity, dataBits, stopBits))
                {
                    serialPort.Open();

                    Console.WriteLine($"Serial port '{serialPort.PortName}' opened successfully.");

                    var buffer = new List<ReadedData>();

                    var rawBuffer = new List<byte>();
                    var byteBuffer = new byte[1];
                    var stringBuffer = new List<byte>();

                    var startDateTime = new DateTime();
                    var stringDateTime = new DateTime();

                    while (true)
                    {
                        serialPort.BaseStream.Read(byteBuffer, 0, 1);

                        if (buffer.Count == 0)
                        {
                            startDateTime = DateTime.Now;
                        }

                        if (rawBuffer.Count == 0)
                        {
                            stringDateTime = DateTime.Now;
                        }

                        rawBuffer.Add(byteBuffer[0]);

                        if (byteBuffer[0] == '\n')
                        {
                            foreach (var symbol in rawBuffer)
                            {
                                if (symbol >= 0x20 && symbol <= 0x7E)
                                {
                                    stringBuffer.Add(symbol);
                                }
                                else if (symbol != '\n' && symbol !='\r')
                                {
                                    stringBuffer.Add(0x3F);
                                }
                            }

                            Console.WriteLine(stringDateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz"));

                            var str = Encoding.ASCII.GetString(stringBuffer.ToArray());

                            Console.WriteLine(str);

                            buffer.Add(new ReadedData { DateTime = stringDateTime, String = str, Raw = rawBuffer });

                            SaveReadedData(startDateTime, buffer);

                            rawBuffer.Clear();
                            stringBuffer.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (Console.CursorLeft != 0)
                {
                    Console.WriteLine();
                }

                ExitWithCode(ExitCode.ReadErr, ex.Message);
            }
        }

        private static void SaveReadedData(DateTime fileDateTime, List<ReadedData> data)
        {
            var homeDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            var saveDirectory = Path.Join(homeDirectory, ".spreader");

            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            var saveFileName = $"spreader_{fileDateTime.ToString("yyyy_MM_dd_HH_mm_ss")}.json";

            var saveFilePath = Path.Join(saveDirectory, saveFileName);

            var json = JsonSerializer.Serialize(data, new JsonSerializerOptions { WriteIndented = true });

            File.WriteAllText(saveFilePath, json);

            Console.WriteLine(json);
        }

        private static void ExitWithCode(ExitCode code, string message = null)
        {
            if (message != null)
            {
                Console.WriteLine(message);
            }
            
            Environment.Exit((int)code);
        }

        private sealed class ReadedData
        {
            public DateTime DateTime { get; set; }

            public string String { get; set; }
            
            public List<byte> Raw { get; set; }
        } 
    }
}
